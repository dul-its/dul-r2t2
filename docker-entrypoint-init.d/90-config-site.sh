#!/bin/bash

set -e

# Get the file name of this script.
# TIP SOURCE:
# https://stackoverflow.com/questions/192319/how-do-i-know-the-script-file-name-in-a-bash-script
file_name=$(basename "$0")

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [[ -v BYPASS_CONFIG_IMPORT ]] && [[ "$BYPASS_CONFIG_IMPORT" =~ ^(yes|YES|true|TRUE) ]]; then
  info "[$file_name] Bypassing all config imports..."
  exit
fi

info "[$file_name] Import DUL-specific configuration..."

# Set our system site UUID to a consistent value; this
# enables configurations to be exported from developers'
# local dev environments for use in other environments.

# Change config directory from default. See:
# https://www.drupal.org/docs/configuration-management/changing-the-storage-location-of-the-sync-directory
drupal_conf_set "\$settings['config_sync_directory']" "/opt/dul/config"

info "[$file_name] Setting the system.site UUID to [$SYSTEM_SITE_UUID]..."
drush config-set "system.site" uuid $SYSTEM_SITE_UUID -y

# Workaround for ConfigImporterException on shortcut_set. See:
# https://www.drupal.org/forum/support/post-installation/2015-12-20/problem-during-import-configuration
# but updated from deprecated entityManager to entityTypeManager
#info "Deleting default shortcut set..."
#drush ev '\Drupal::entityTypeManager()->getStorage("shortcut_set")->load("default")->delete();'

# Import all config YML files that were copied from
# our code repo to the config directory (see Dockerfile).
# See https://drushcommands.com/drush-9x/config/config:import/

# Uncomment following line once config directory is set up
info "[$file_name] Import partial set of configurations..."
drush config-import --source $DRUPAL_DUL_CONFIG --partial -y
# drush config-import --source $DRUPAL_CONFIG_SYNC_DIR --partial -y

info "[$file_name] Export config to Drupal's sync directory for staging...[${DRUPAL_CONFIG_SYNC_DIR}]"
drush config-export --destination $DRUPAL_CONFIG_SYNC_DIR -y

# If a custom GA account is specified for this env, override the value configured in
# the google_analytics module (see config/google_analytics.settings.yml).
# Note that this gets written to sites/default/settings.php.
#if [ ! -z "$GOOGLE_ANALYTICS_ID" ]; then
#  drupal_conf_set "\$config['google_analytics.settings']['account']" "$GOOGLE_ANALYTICS_ID" no
#fi

info "[$file_name] Done with configuration import!"
