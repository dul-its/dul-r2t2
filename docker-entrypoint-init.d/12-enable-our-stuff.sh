#!/bin/bash

set -e

# Get the file name of this script.
# TIP SOURCE:
# https://stackoverflow.com/questions/192319/how-do-i-know-the-script-file-name-in-a-bash-script
file_name=$(basename "$0")

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

# Add logic to bypass when DRUPAL_SKIP_BOOTSTRAP=yes
if [[ -v DRUPAL_SKIP_BOOTSTRAP ]] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "[$file_name]: skipping because we're not bootstraping..."
  exit
fi

info "[$file_name] Enable 'samlauth' module..."
drush install samlauth
drush install samlauth_user_roles

info "[$file_name]: Enable 'easy_breadcrumb' module..."
drush install easy_breadcrumb

info "[$file_name] Enable 'r2t2_content_type' module..."
drush install r2t2_content_type

if [[ -v IMPORT_LEGACY_DATA ]] && [[ "$IMPORT_LEGACY_DATA" == "yes" ]]; then
  info "[$file_name] Importing intial R2T2 data..."
  drush import-r2t2-items --remove-existing --all r2t2_updated.json
fi

# DEPRECATED in favor of
# group manager role + adding roles/group mapping to
# samlauth_user_roles
#info "[$file_name] Creating R2T2 superusers and admins..."
#drush r2-users

info "[$file_name] Enable 'Vivid' theme..."

drush theme:install bootstrap5 -y

drush theme:install vivid -y
drush config-set system.theme default vivid -y

drush install samlauth
drush config-set samlauth.authentication login_redirect_url "/recommendations" -y
drush config-set samlauth.authentication unique_id_attribute "eduPersonPrincipalName" -y
drush config-set samlauth.authentication create_users true -y
drush config-set samlauth.authentication sync_name false -y
drush config-set samlauth.authentication sync_mail true -y
drush config-set samlauth.authentication user_name_attribute "uid" -y
drush config-set samlauth.authentication user_mail_attribute "mail" -y


info "done!"
