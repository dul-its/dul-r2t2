#!/bin/bash

set -e

# Get the file name of this script.
# TIP SOURCE:
# https://stackoverflow.com/questions/192319/how-do-i-know-the-script-file-name-in-a-bash-script
file_name=$(basename "$0")

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

# Update our installed modules when we're not bootstrapping.
# Otherwise, install them on a clean setup.
if [[ -v DRUPAL_SKIP_BOOTSTRAP ]] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
  info "10-install-our-stuff: attempt to update installed modules -- r2t2_content_type and vivid_theme"
  composer update 'drupal/bootstrap5'
  composer update 'dul-drupal/r2t2_content_type'
  composer update 'dul-drupal/vivid_theme'
  composer update 'drupal/samlauth'
  composer update 'drupal/easy_breadcrumb'
  #composer require 'drupal/easy_breadcrumb:^2.0'
fi

info "[$file_name] Done!"
