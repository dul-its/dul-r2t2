#!/bin/bash

set -e

# Get the file name of this script.
# TIP SOURCE:
# https://stackoverflow.com/questions/192319/how-do-i-know-the-script-file-name-in-a-bash-script
file_name=$(basename "$0")

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

# Add logic to bypass when DRUPAL_SKIP_BOOTSTRAP=yes
#if [[ -v DRUPAL_SKIP_BOOTSTRAP ]] && [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(yes|YES) ]]; then
#  info "[$file_name]: skipping because we're not bootstraping..."
#  exit
#fi

cd /opt/bitnami/drupal
info "[$file_name]: COMPOSER var is set to '${COMPOSER}'"

## No harm is done to the composer repository setup if these 
## entries are already in place.
composer config repositories.dul-drupal/r2t2_content_type git \
  https://gitlab.oit.duke.edu/dul-drupal/r2t2_content_type.git

composer config repositories.dul-drupal/vivid_theme git \
  https://gitlab.oit.duke.edu/dul-drupal/vivid_theme.git

info "Attempting to set R/W permissions on sites/default/*..."
chmod -R a+rw /bitnami/drupal/sites/default

exec "$@"
