#!/bin/bash

set -e

# load logging library
source /opt/bitnami/scripts/liblog.sh
source /opt/bitnami/scripts/libdrupal.sh

if [[ ! -v DRUPAL_SKIP_BOOTSTRAP ]] || [[ "$DRUPAL_SKIP_BOOTSTRAP" =~ ^(no|NO) ]]; then
  info "We're bootstrapping, so no updates here!"
  exit
fi

# info "[11-install-our-stuff] Installing Bootstrap5, r2t2_content_type and vivid_theme..."

# composer update 'dul-drupal/r2t2_content_type:dev-main' \
#  && composer update 'dul-drupal/vivid_theme:dev-main'
