{{/*
Expand the name of the chart.
*/}}
{{- define "transparency.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "transparency.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "transparency.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "transparency.labels" -}}
helm.sh/chart: {{ include "transparency.chart" . }}
{{ include "transparency.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "transparency.selectorLabels" -}}
app.kubernetes.io/name: {{ include "transparency.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "transparency.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "transparency.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create the fullname of the database
*/}}
{{- define "mariadb.fullname" -}}
{{- printf "%s-%s" .Release.Name "mariadb" | trunc 63 | trimSuffix "-" -}}
{{- end }}

{{/*
Create the name of database secret.
*/}}
{{- define "mariadb.secretname" -}}
{{- printf "%s-mariadb" .Chart.Name -}}
{{- end }}

{{/*
Create the fullname of the primary data volume.
*/}}
{{- define "transparency.datavolume" -}}
{{- printf "volume-dul-r2t2-datastore" -}}
{{- end }}

{{- define "transparency.configsyncvolume" }}
{{- printf "volume-config-sync" }}
{{- end }}

{{/*
Create the name of the peristent volume for managing backups
*/}}
{{- define "transparency-backups.volumename" }}
{{- printf "dul-datastore-backups" }}
{{- end }}
