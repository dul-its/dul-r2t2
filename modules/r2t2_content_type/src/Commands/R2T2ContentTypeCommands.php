<?php

namespace Drupal\r2t2_content_type\Commands;

use Drush\Commands\DrushCommands;
use Drush\Drush;
use Symfony\Component\Yaml\Yaml;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;

/**
 * A Drush commandfile
 *
 */
class R2T2ContentTypeCommands extends DrushCommands {
  /**
   * Import data from remote r2t2 server.
   *
   * @param $filename
   *   Relative (to module/data) or absolute filename path
   * @param array $options
   *   An associative array of options from CLI
   * @usage r2t2-importJsonData
   *
   * @command r2t2:importJsonData
   * @aliases import-r2t2-items
   */
  public function importJsonData($filename = '', $options = ['content-type' => 'report_recommendation', 
                                                             'rows' => 10,
                                                             'remove-existing' => FALSE]) {
    $dataDir = \Drupal::service('extension.list.module')->getPath('r2t2_content_type') . '/data';

    if ( !$filename ) {
      // assume our 'r2t2_current.json' file
      $filename = $dataDir . '/r2t2_current.json';
    } else {
      if (preg_match("/^\//", $filename) == 0) {
        $filename = $dataDir . '/' . $filename;
      }
      // otherwise, assume absolute file system path.
    }
    $this->logger()->notice( "filename = [" . $filename . "]" );

    if ( $options['remove-existing'] ) {
      # TIP SOURCE:
      # https://drupal.stackexchange.com/questions/537/how-to-delete-all-nodes-of-a-given-content-type
      $this->logger()->notice( "Removing existing 'report_recommendation' nodes...");
      $storage_handler = \Drupal::entityTypeManager()->getStorage("node");
      $entities = $storage_handler->loadByProperties(["type" => $options['content-type']]);
      $storage_handler->delete($entities);
    }

    // Decode as an associative array.
    // (json_decode returns 'stdClass' by default)
    $fileContents = file_get_contents($filename);
    $items = json_decode($fileContents, TRUE, 512, JSON_THROW_ON_ERROR);

    $this->logger()->notice( count($items) );

    $this->logger()->notice( "importing data now" );
    $i = 0;
    while ( $i < $options['rows'] && $items[$i] ) {
      $node = Node::create(['type' => $options['content-type']]);
      $node->langcode = 'en';
      $node->body = $items[$i][0];
      $node->field_recommendation_source = $items[$i][1];
      $node->field_recommendation_progress = $items[$i][6];
      $node->field_recommendation_source = $items[$i][1];
      $node->field_recommendation_status = $items[$i][5];
      $node->field_repsonsible_party = $items[$i][4];
      $node->promote = 0;
      $node->uid = 1;
      $node->title = 'Untitled';
      $node->save();
      $i++;
    }

    /*
    $node = Node::create(['type' => 'report_recommendation']);
    $node->langcode = 'en';
    $node->body = 'Here we go with a new suggestion';
    $node->promote = 0;
    $node->uid = 1;
    $node->title = 'New Suggestion for 2023';
    $node->field_item_authors = 'Me and friends';
    $node->save();
     */
  }
}
