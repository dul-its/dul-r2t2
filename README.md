# dul-r2t2

[[_TOC_]]

## Getting started

## Local Development

```bash
$ git clone git@gitlab.oit.duke.edu:dul-its/dul-r2t2.git

$ cd /path/to/dul-r2t2

$ mkdir drupal-data
$ chmod a+rwx drupal-data

$ cp docker-compose.example docker-compose.yml
$ cp Dockerfile-example Dockerfile

$ docker-compose up --build
```
  
The resulting site will feature:  
* A `report_recommendation` content type with defined fields,
  * Drupal's default `body` field labeled as `recommendation`
  * `recommendation_source`
  * `recommendation_category` (leverages a defined taxomony vocabulary -- see below)
  * `eg_decision` (Executive Group Decision)
  * `responsible_party` (Responsible Group/Dept/Person)
  * `recommendation_status`
  * `recommendation_progress`
  * `campus_partnerships`
* A `report_recommendation_category` Taxonomy Vocabulary
  * This vocabulary's terms are referred to by the `recommendation_category` field
  
---
### Module and Theme Development
Once the stack is running, and you believe it necessary to work on either a 
module or theme (or both! why not...), you'll likely want to run this command first:  
  
```bash
$ [sudo] chmod -R a+rw drupal-data/modules drupal-data/themes
```
  
Now, you should be able to use your favorite editor (`vim`, right?) to manipulate files locally.  

---
### Running `drush` Commands
More often then not, you'll need to run `drush` commands against your local "drupal" container -- 
especially true during theme development. So:  
  
```bash
$ docker exec -it -u 0 drupal drush ...
```
  
This assumes the container name is `drupal`.  
  
The `-u 0` flag will ensure you're running commands as the root user.  

---
### Adding Recommendation Items (via Drush)
With the stack running, you can add the first 10 rows of "live" R2T2 data by:
```bash
$ docker exec -it [-u 0] drupal drush import-r2t2-items [--remove-existing] [--rows XX]

# or

$ docker exec -it [-u 0] drupal drush import-r2t2-items </path/to/your/file.json> [--remove-existing]
```

---
### Resetting The Stack
```bash
$ [sudo] rm -rf drupal-data/*
```
  
This will clear the persistence used by the Drupal container.  
  
This will NOT clear the database persistence, however. To do that...  
  
```bash
$ docker volume rm dul-r2t2_mariadb_data
```
### Under The Hood (DEPRECATED)
Feel free to inspect `modules/r2t2_content_type/r2t2_content_type.install` to see how the 
taxonomy and its terms are created.
  
**Content Type & Field Initialization**  
The YAML config files located at `modules/r2t2_content_type/config/install` are processed 
when the module is installed/enabled.

## OKD Deployment
The Drupal-based instance deployed to OIT's Kubernetes cluster (OKD) through the use 
of a Helm chart (located at `/projectroot/transparency`).  
  
### Initial Setup
The initial setup for this Helm chart approach is similar to that of Staff Directory and Spacefinder.  
  
1. Create a "generic" secret to store database passwords used by the application.
2. Create a TLS secret for the certificate and private key.
3. Build the Docker image that will be saved as an `imagestream`
  * Refer to this utility script [`build-okd-imgstr`](https://gitlab.oit.duke.edu/devops/bash-scripts/-/blob/main/build-okd-imgstr).

### Install the Chart / Deploy The Application
```bash
$ cd /path/to/dul-r2t2/transparency

$ oc login ... (go to the OKD console and obtain your token)

$ docker login -u openshift -p $(oc whoami -t) registry.apps.[dev|prod].okd4.fitz.cloud.duke.edu

$ build-okd-imgstr -f ../.docker/Dockerfile [-y] transparency

$ helm install (stage|prod) . [--set-file tls.crt=../(stage|prod).pem] [--set-file tls.key=../(stage|prod).key] \
  --values (stage|prod).yaml [--dry-run]
```
### Reset Everything
Coming soon

## OKD / Kubernetes

### Accessing Application Terminal
**Staging**  
https://console.apps.dev.okd4.fitz.cloud.duke.edu/k8s/cluster/projects/dul-r2t2-stage.  

**STEPS**
1. Locate the **Inventory** section, then click the "Pods" link (this may be proceeded by a numerical value).  
2. Locate and click the pod whose name starts with `[release_name]-transparency` (where `release_name` is "stage" or "prod").  
3. Locate and click the "Terminal" tab.  
  
From there, commands like `drush ...` or `composer ...` are available.

