#!/usr/bin/perl

use strict;
use warnings;

use Carp;
use Data::Dumper;
use JSON::XS;
use Text::CSV qw( csv );

binmode STDOUT, ':utf8:';

croak "File argument required!" unless $ARGV[0];
my ($filepath) = @ARGV;

my @rows;
my $csv = Text::CSV->new ({ binary => 1, auto_diag => 1 });
open my $fh, "<:encoding(utf8)", "$filepath" or die "$filepath: $!";

my @export = ();

while (my $row = $csv->getline ($fh)) {
  $row->[0] =~ s/\r?\n//g;
  $row->[1] =~ s/\r?\n//g;
  $row->[2] =~ s/\r?\n/ /g;

  push @export, $row;
}
close $fh;

# create our JSON data struct
my $json = JSON::XS
  ->new
  ->pretty(1)
  ->utf8
  ->space_after
  ->canonical(1);

print $json->encode(\@export);

